﻿using Arsenal.Migrations.Extensions;
using FluentMigrator;

namespace Arsenal.Migrations
{
	[Migration(2)]
	public class CreateTeamsTable : Migration
	{
		public override void Up()
		{
			Create.Table("Teams")
				.WithColumn("Id").AsInt64().NotNullable().PrimaryKey().Identity()
				.WithColumn("Name").AsString(250).NotNullable().Indexed()
				.WithTimestamps();
		}

		public override void Down()
		{
			Delete.Table("Teams");
		}
	}
}