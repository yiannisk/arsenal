﻿using Arsenal.Migrations.Extensions;
using FluentMigrator;

namespace Arsenal.Migrations
{
	[Migration(1)]
	public class CreateSystemTable : Migration
	{
		public override void Up()
		{
			Create.Table("Systems")
				.WithColumn("Id").AsInt64().NotNullable().PrimaryKey().Identity()
				.WithColumn("Name").AsString(100).NotNullable().Indexed()
				.WithColumn("Description").AsString(2048).Nullable().Indexed()
				.WithColumn("TeamId").AsInt64().NotNullable().Indexed()
				.WithTimestamps();
		}

		public override void Down()
		{
			Delete.Table("Systems");
		}
	}
}