﻿using Arsenal.Migrations.Extensions;
using FluentMigrator;

namespace Arsenal.Migrations
{
	[Migration(5)]
	public class CreateApplicationDependenciesTable : Migration
	{
		public override void Up()
		{
			Create.Table("ApplicationDependencies")
				.WithColumn("Id").AsInt64().NotNullable().PrimaryKey().Identity()
				.WithColumn("ApplicationId").AsInt64().NotNullable().Indexed()
				.WithColumn("DependencyVersionId").AsInt64().NotNullable().Indexed()
				.WithColumn("LatestVersionId").AsInt64().Nullable()
				.WithColumn("DesiredVersionId").AsInt64().Nullable()
				.WithColumn("CanBeUpdated").AsBoolean().Nullable()
				.WithColumn("Reason").AsString(2^11).Nullable().Indexed()
				.WithColumn("Source").AsString(2^11).Nullable().Indexed()
				.WithTimestamps();
		}

		public override void Down()
		{
			Delete.Table("ApplicationDependencies");
		}
	}
}