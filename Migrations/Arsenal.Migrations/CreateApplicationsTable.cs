﻿using Arsenal.Migrations.Extensions;
using FluentMigrator;

namespace Arsenal.Migrations
{
	[Migration(3)]
	public class CreateApplicationsTable : Migration
	{
		public override void Up()
		{
			Create.Table("Applications")
				.WithColumn("Id").AsInt64().NotNullable().PrimaryKey().Identity()
				.WithColumn("Name").AsString(100).NotNullable().Indexed()
				.WithColumn("Description").AsString(2048).Nullable().Indexed()
				.WithColumn("Repository").AsString(250).Nullable().Indexed()
				.WithColumn("RepositoryType").AsString(50).Nullable().Indexed()
				.WithTimestamps();
		}

		public override void Down()
		{
			Delete.Table("Applications");
		}
	}
}