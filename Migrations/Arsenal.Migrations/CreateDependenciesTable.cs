﻿using Arsenal.Migrations.Extensions;
using FluentMigrator;

namespace Arsenal.Migrations
{
	[Migration(4)]
	public class CreateDependenciesTable : Migration
	{
		public override void Up()
		{
			Create.Table("Dependencies")
				.WithColumn("Id").AsInt64().NotNullable().PrimaryKey().Identity()
				.WithColumn("Name").AsString(100).NotNullable().Indexed()
				.WithColumn("ReleaseNotes").AsMaxString().Nullable()
				.WithTimestamps();
		}

		public override void Down()
		{
			Delete.Table("Dependencies");
		}
	}
}