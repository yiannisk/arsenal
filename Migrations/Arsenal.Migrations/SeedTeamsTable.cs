﻿using FluentMigrator;

namespace Arsenal.Migrations
{
	[Migration(7)]
	public class SeedTeamsTable : Migration
	{
		public override void Up()
		{
			Execute.Sql(@"INSERT INTO Teams (Name) 
									 SELECT 'LeWeb'
						   UNION ALL SELECT 'Services'
						   UNION ALL SELECT 'Flights'
						   UNION ALL SELECT 'Mobile'
						   UNION ALL SELECT 'Panoramix'
						   UNION ALL SELECT 'Admins'
						   UNION ALL SELECT 'Data';");
		}

		public override void Down()
		{
			Execute.Sql("TRUNCATE TABLE Teams;");
		}
	}
}