﻿using FluentMigrator;
using FluentMigrator.Builders.Create.Table;

namespace Arsenal.Migrations.Extensions
{
	internal static class MigratorExtensions
	{
		public static ICreateTableColumnOptionOrWithColumnSyntax AsMaxString(
			this ICreateTableColumnAsTypeSyntax createTableColumnAsTypeSyntax)
		{
			return createTableColumnAsTypeSyntax.AsString(int.MaxValue);
		}

		public static ICreateTableWithColumnSyntax WithTimestamps(this ICreateTableWithColumnSyntax root)
		{
			return root.
				WithColumn("CreatedAt").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime).
				WithColumn("UpdatedAt").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime);
		}
	}
}