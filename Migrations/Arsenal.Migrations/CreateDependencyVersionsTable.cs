﻿using Arsenal.Migrations.Extensions;
using FluentMigrator;

namespace Arsenal.Migrations
{
	[Migration(6)]
	public class CreateDependencyVersionsTable : Migration
	{
		public override void Up()
		{
			Create.Table("DependencyVersions")
				.WithColumn("Id").AsInt64().NotNullable().PrimaryKey().Identity()
				.WithColumn("DependencyId").AsInt64().NotNullable().Indexed()
				.WithColumn("VersionString").AsString(100).NotNullable().Indexed()
				.WithColumn("MajorVersion").AsInt32().NotNullable().WithDefault(0).Indexed()
				.WithColumn("MinorVersion").AsInt32().Nullable().Indexed()
				.WithColumn("MajorRevision").AsInt32().Nullable().Indexed()
				.WithColumn("MinorRevision").AsInt32().Nullable().Indexed()
				.WithTimestamps();
		}

		public override void Down()
		{
			Delete.Table("DependencyVersions");
		}
	}
}