﻿using System;
using System.Collections.Generic;
using Arsenal.DataAccess.Models;
using Arsenal.DataAccess.Repositories.Interfaces;
using Arsenal.Service.Controllers;
using Arsenal.Service.Factories.Models.Interfaces;
using Arsenal.Service.Factories.ViewModels.Interfaces;
using Arsenal.Service.Models;
using Moq;
using NUnit.Framework;

namespace Arsenal.Service.Tests.Controllers
{
	[TestFixture]
	public class ApplicationControllerTests
	{
		#region GET /applications

		[Test]
		public void TestGetAllApplicationsFailsOnDatabaseFail()
		{
			var applicationsRepositoryMock = new Mock<IApplicationsRepository>(MockBehavior.Loose);

			applicationsRepositoryMock.Setup(x => x.GetAll()).Throws(new Exception()).Verifiable();

			var applicationDtoFactoryMock = new Mock<IApplicationDtoFactory>(MockBehavior.Loose);
			var applicationFactoryMock = new Mock<IApplicationFactory>(MockBehavior.Loose);
			
			var applicationsController = new ApplicationsController(applicationsRepositoryMock.Object,
																	applicationDtoFactoryMock.Object, 
																	applicationFactoryMock.Object);

			Assert.Throws<Exception>(() => applicationsController.GetAllApplications());

			applicationsRepositoryMock.VerifyAll();
			applicationDtoFactoryMock.VerifyAll();
			applicationFactoryMock.VerifyAll();
		}

		[Test]
		public void TestGetAllApplicationsHappyPath()
		{
			var applicationsRepositoryMock = new Mock<IApplicationsRepository>(MockBehavior.Loose);
			applicationsRepositoryMock.Setup(x => x.GetAll())
				.Returns(new List<Application> {new Application {Name = "app"}})
				.Verifiable();

			var applicationDtoFactoryMock = new Mock<IApplicationDtoFactory>(MockBehavior.Loose);
			applicationDtoFactoryMock.Setup(x => x.CreateMany(It.IsAny<IList<Application>>()))
				.Returns(new List<ApplicationDto>())
				.Verifiable();

			var applicationFactoryMock = new Mock<IApplicationFactory>(MockBehavior.Loose);

			var applicationsController = new ApplicationsController(applicationsRepositoryMock.Object, 
																	applicationDtoFactoryMock.Object, 
																	applicationFactoryMock.Object);

			Assert.DoesNotThrow(() => applicationsController.GetAllApplications());

			applicationsRepositoryMock.VerifyAll();
			applicationDtoFactoryMock.VerifyAll();
		}

		[Test]
		public void TestGetAllApplicationsNoDataReturned()
		{
			var applicationsRepositoryMock = new Mock<IApplicationsRepository>(MockBehavior.Loose);
			applicationsRepositoryMock.Setup(x => x.GetAll()).Returns(null as List<Application>).Verifiable();

			var applicationDtoFactoryMock = new Mock<IApplicationDtoFactory>(MockBehavior.Loose);
			applicationDtoFactoryMock.Setup(x => x.CreateMany(It.IsAny<IList<Application>>()))
				.Returns(null as List<ApplicationDto>)
				.Verifiable();

			var applicationFactoryMock = new Mock<IApplicationFactory>(MockBehavior.Loose);

			var controller = new ApplicationsController(applicationsRepositoryMock.Object, 
																	applicationDtoFactoryMock.Object,
																	applicationFactoryMock.Object);

			Assert.DoesNotThrow(() => controller.GetAllApplications());

			applicationsRepositoryMock.VerifyAll();
			applicationDtoFactoryMock.VerifyAll();
		}

		#endregion

		#region POST /applications

		[Test]
		public void CreateApplicationHappyPath()
		{
			var applicationDtoFactoryMock = new Mock<IApplicationDtoFactory>();
			applicationDtoFactoryMock.Setup(x => x.Create(It.IsAny<Application>())).Returns(new ApplicationDto()).Verifiable();
			var applicationFactoryMock = new Mock<IApplicationFactory>();
			applicationFactoryMock.Setup(x => x.Create(It.IsAny<ApplicationDto>())).Returns(new Application()).Verifiable();

			var controller = new ApplicationsController(null, applicationDtoFactoryMock.Object, applicationFactoryMock.Object);
			controller.CreateApplication(new ApplicationDto());

			applicationDtoFactoryMock.VerifyAll();
			applicationFactoryMock.VerifyAll();
		}

		#endregion
	}
}