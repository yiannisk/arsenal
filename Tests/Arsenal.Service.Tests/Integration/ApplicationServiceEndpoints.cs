﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using Arsenal.Service.Models;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Arsenal.Service.Tests.Integration
{
	[TestFixture]
	public class ApplicationServiceEndpoints
	{
		private const string baseUrl = "http://localhost/Arsenal.Service/api/applications";

		[Test, Ignore]
		public void TestCreateApplication()
		{
			var request = CreateRequest(baseUrl, "Fixtures\\ETravel.Cars.Cli.csv");
			var response = GetResponse(request);
			Console.WriteLine(response);
		}

		private string GetResponse(HttpWebRequest request)
		{
			using (var response = request.GetResponse() as HttpWebResponse)
				using (var reader = new StreamReader(response.GetResponseStream()))
					return reader.ReadToEnd();
		}

		private HttpWebRequest CreateRequest(string baseUrl, string fixture)
		{
			var request = (HttpWebRequest) WebRequest.Create(baseUrl);
			request.Method = "POST";
			request.Accept = request.ContentType = "application/json";

			var applicationDto = GetApplicationDtoFromFixture(fixture);
			using(var writer = new StreamWriter(request.GetRequestStream()))
				writer.Write(JsonConvert.SerializeObject(applicationDto));

			return request;
		}

		private ApplicationDto GetApplicationDtoFromFixture(string fixture)
		{
			var name = Path.GetFileNameWithoutExtension(fixture);
			var dependencies = File.ReadLines(fixture).Skip(1)
				.Select(x =>
				{
					var fields = x.Split(',');
					return new ApplicationDependencyDto
					{
						CurrentVersion = fields[1],
						Name = fields[0],
						UpdatedAt = DateTime.Now
					};
				})
				.ToList();

			return new ApplicationDto
			{
				Name = name,
				Dependencies = dependencies
			};
		}
	}
}