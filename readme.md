# Arsenal

A services and resources inventory service, written in C#.

---

## Stack

### Arsenal.Service

* Net 4.5
* Web API 2
* [FluentMigrator](https://github.com/schambers/fluentmigrator)
* [Dapper](https://github.com/StackExchange/dapper-dot-net)
* [Dapper.SimpleCRUD](https://github.com/ericdc1/Dapper.SimpleCRUD)

---

## Migrations

To execute migrations against your custom data source:

1. Edit `up.bat` and `down.bat` to set the connection string.
2. Build the project.
3. Open a console to `Projects\Arsenal.Migrations\bin\<Configuration>\`
4. Execute `up` or `down <number of steps>`

---

## TODO

* Data Migrations / seeds (prototyping in place)
* UX Site + Stack
* Roles + Rights
* OAuth with Google
* Model Relationships maybe (https://gist.github.com/Lobstrosity/1133111)
* Dependencies Tracking Endpoints: POST + PUT /dependencies - some thought needed here.