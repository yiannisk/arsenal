﻿joint.shapes.architecture = {};

joint.shapes.architecture.Folder = joint.shapes.basic.Generic.extend({
    markup: '<g class="scalable"><rect x="-20" y="-10" width="100" height="100" fill="white" /><path d="M57.49,27H54v-7.268C54,18.226,52.774,17,51.268,17H48v-2.414L32.414,0H8v10H2.732C1.226,10,0,11.226,0,12.732v45.23l0.058,0.002c0.078,0.367,0.234,0.719,0.471,1.029C1.018,59.634,1.76,60,2.565,60h44.759c1.156,0,2.174-0.779,2.45-1.813L60,30.149v-0.177C60,28.25,58.944,27,57.49,27z M31,2.288c0-0.106,0.129-0.16,0.204-0.084l14.592,13.593C45.871,15.872,45.818,16,45.712,16H31V2.288z M16,5h10c0.552,0,1,0.447,1,1s-0.448,1-1,1H16c-0.552,0-1-0.447-1-1S15.448,5,16,5z M16,12h10c0.552,0,1,0.447,1,1s-0.448,1-1,1H16c-0.552,0-1-0.447-1-1S15.448,12,16,12z M16,19h24c0.552,0,1,0.447,1,1s-0.448,1-1,1H16c-0.552,0-1-0.447-1-1S15.448,19,16,19z M10.281,28.813L2,51.526V12.732C2,12.328,2.328,12,2.732,12H8v19l1.666-4.164C9.868,26.331,10.356,26,10.9,26H46h2v-7h3.268C51.672,19,52,19.328,52,19.732V27h-5H12.731C11.575,27,10.557,27.779,10.281,28.813z" style="opacity:1; fill:#000000; fill-opacity:1; fill-rule:nonzero;" /></g><circle r="3" stroke="black" fill="lightgreen" stroke-width="2"/><text/>',
    defaults: joint.util.deepSupplement({
        type: 'architecture.Folder',
        size: { width: 60, height: 60 },
        attrs: {
            'text': {
                'font-size': 12,
                text: 'folder',
                'ref-x': .5,
                'ref-y': .99,
                ref: 'path',
                'y': 15,
                'y-alignment': 'bottom',
                'x-alignment': 'middle',
                fill: 'black'
            },
            'circle': {
                ref: 'rect',
                'ref-x': .85,
                'ref-y': .6
            }
        }
    }, joint.shapes.basic.Generic.prototype.defaults)
});

joint.shapes.architecture.Demon = joint.shapes.basic.Generic.extend({
    markup: '<g class="scalable"><rect x="-40" y="-30" width="550" height="530" fill="white" /><path d="M360.909,17.934H24.061C10.767,17.934,0,28.713,0,41.995V258.54c0,13.293,10.767,24.061,24.061,24.061h60.152c7.711,0,12.03-5.45,12.03-12.03c0-6.581-4.09-12.187-12.03-12.187H38.738c-8.036,0-14.557-6.52-14.557-14.557V57.093c0-8.036,6.52-14.557,14.557-14.557l307.627-0.373c8.036,0,14.557,6.52,14.557,14.557v187.107c0,8.036-6.52,14.557-14.557,14.557H209.556l62.413-63.303c4.692-4.752,4.692-12.439,0-17.191c-4.704-4.74-12.319-4.74-17.011,0l-83.009,84.2c-4.692,4.74-4.692,12.439,0,17.191c0,0,0,0,0.012,0l82.997,84.2c4.692,4.74,12.319,4.74,17.011,0c4.692-4.752,4.692-12.439,0-17.179l-62.774-63.688h151.714c13.293,0,24.061-10.767,24.061-24.061V42.007C384.97,28.713,374.203,17.934,360.909,17.934z" style="opacity:1; fill:#000000; fill-opacity:1; fill-rule:nonzero;"/></g><circle r="3" stroke="black" fill="lightgreen" stroke-width="2"/><text/>',
    defaults: joint.util.deepSupplement({
        type: 'architecture.Demon',
        size: { width: 60, height: 60 },
        attrs: {
            'text': {
                'font-size': 12,
                text: 'demon',
                'ref-x': .5,
                'ref-y': .96,
                ref: 'path',
                'y': 15,
                'y-alignment': 'bottom',
                'x-alignment': 'middle',
                fill: 'black'
            },
            'circle': {
                ref: 'rect',
                'ref-x': .9,
                'ref-y': .53
            }
        }
    }, joint.shapes.basic.Generic.prototype.defaults)
});

joint.shapes.architecture.Database = joint.shapes.basic.Generic.extend({
    markup: '<g class="scalable"><rect x="-20" y="-10" width="140" height="180" fill="white" /><g id="shape"><path d="M47.561,0C25.928,0,8.39,6.393,8.39,14.283v11.72c0,7.891,17.538,14.282,39.171,14.282c21.632,0,39.17-6.392,39.17-14.282v-11.72C86.731,6.393,69.193,0,47.561,0z" style="opacity:1; fill:#000000; fill-opacity:1; fill-rule:nonzero;"/><path d="M47.561,47.115c-20.654,0-37.682-5.832-39.171-13.227c-0.071,0.353,0,19.355,0,19.355c0,7.892,17.538,14.283,39.171,14.283c21.632,0,39.17-6.393,39.17-14.283c0,0,0.044-19.003-0.026-19.355C85.214,41.284,68.214,47.115,47.561,47.115z" style="opacity:1; fill:#000000; fill-opacity:1; fill-rule:nonzero;"/><path d="M86.694,61.464c-1.488,7.391-18.479,13.226-39.133,13.226S9.875,68.854,8.386,61.464L8.39,80.82c0,7.891,17.538,14.282,39.171,14.282c21.632,0,39.17-6.393,39.17-14.282L86.694,61.464z" style="opacity:1; fill:#000000; fill-opacity:1; fill-rule:nonzero;"/></g></g><circle r="3" stroke="black" fill="lightgreen" stroke-width="2"/><text/>',
    defaults: joint.util.deepSupplement({
        type: 'architecture.Database',
        size: { width: 50, height: 60 },
        attrs: {
            'text': {
                'font-size': 12,
                text: 'database',
                'ref-x': .5,
                'ref-y': .99,
                ref: '#shape',
                y: 15,
                'y-alignment': 'bottom',
                'x-alignment': 'middle',
                fill: 'black'
            },
            'circle': {
                ref: 'rect',
                'ref-x': .9,
                'ref-y': .5
            }
        }
    }, joint.shapes.basic.Generic.prototype.defaults)
});