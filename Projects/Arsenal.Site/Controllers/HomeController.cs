﻿using System.Web.Mvc;

namespace Arsenal.Site.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}
	}
}