﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Arsenal.Site.Startup))]
namespace Arsenal.Site
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
