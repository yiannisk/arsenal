﻿using System.Web.Optimization;

namespace Arsenal.Site
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
			bundles.Add(new StyleBundle("~/Content/css")
				.Include("~/Content/foundation.min.css", "~/Content/app.css"));

			bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/vendor/modernizr.js"));
			
			bundles.Add(new ScriptBundle("~/bundles/foundation")
				.Include("~/Scripts/vendor/jquery.js", "~/Scripts/foundation.min.js", "~/Scripts/app.js"));
        }
    }
}
