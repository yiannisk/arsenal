﻿using System.Collections.Generic;
using System.Web.Http;
using Arsenal.DataAccess.Repositories.Interfaces;
using Arsenal.Service.Factories.Models.Interfaces;
using Arsenal.Service.Factories.ViewModels.Interfaces;
using Arsenal.Service.Models;

namespace Arsenal.Service.Controllers
{
	public class ApplicationsController : ApiController
	{
		public ApplicationsController(
			IApplicationsRepository applicationsRepository,
			IApplicationDtoFactory applicationDtoFactory,
			IApplicationFactory applicationFactory)
		{
			ApplicationsRepository = applicationsRepository;
			ApplicationDtoFactory = applicationDtoFactory;
			ApplicationFactory = applicationFactory;
		}

		private IApplicationsRepository ApplicationsRepository { get; set; }
		private IApplicationDtoFactory ApplicationDtoFactory { get; set; }
		private IApplicationFactory ApplicationFactory { get; set; }

		public IEnumerable<ApplicationDto> GetAllApplications()
		{
			return ApplicationDtoFactory.CreateMany(ApplicationsRepository.GetAll());
		}

		public ApplicationDto CreateApplication(ApplicationDto applicationDto)
		{
			return ApplicationDtoFactory.Create(ApplicationFactory.Create(applicationDto));
		}
	}
}