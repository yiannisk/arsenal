﻿using System.Collections.Generic;
using System.Web.Http;
using Arsenal.DataAccess.Repositories.Interfaces;

namespace Arsenal.Service.Controllers
{
	public class SystemsController : ApiController
	{
		public SystemsController(ISystemsRepository systemsRepository)
		{
			SystemsRepository = systemsRepository;
		}

		private ISystemsRepository SystemsRepository { get; set; }

		public IEnumerable<DataAccess.Models.System> GetAllSystems()
		{
			return SystemsRepository.GetAll();
		}
	}
}