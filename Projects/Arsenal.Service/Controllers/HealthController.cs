﻿using System;
using System.Web.Http;
using Arsenal.DataAccess.Helpers;

namespace Arsenal.Service.Controllers
{
    public class HealthController : ApiController
    {
        private IDatabaseHelpers DatabaseHelpers { get; set; }

        public HealthController(IDatabaseHelpers databaseHelpers)
        {
            DatabaseHelpers = databaseHelpers;
        }

        [Route("health/status")]
        public dynamic GetStatus()
        {
            if (DatabaseHelpers.CanConnect())
                return Ok();

            return InternalServerError();
        }
	}
}