﻿using System;

namespace Arsenal.Service.Extensions
{
	public static class StringExtensions
	{
		public static Version ToVersion(this string version)
		{
			if (string.IsNullOrWhiteSpace(version)) return null;

			var product = new Version();

			var versionParts = version.Split(new[] { '.' }, StringSplitOptions.None);
			product.MajorVersion = int.Parse(versionParts[0]);

			if (versionParts.Length > 1)
				product.MinorVersion = int.Parse(versionParts[1]);

			if (versionParts.Length > 2)
				product.MajorRevision = int.Parse(versionParts[2]);

			if (versionParts.Length > 3)
				product.MinorRevision = int.Parse(versionParts[3]);

			return product;
		}
	}

	public class Version
	{
		public int MajorVersion { get; set; }
		public int? MinorVersion { get; set; }
		public int? MajorRevision { get; set; }
		public int? MinorRevision { get; set; }
	}
}