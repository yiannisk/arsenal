﻿using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace Arsenal.Service.Handlers
{
	/// <summary>
	///     Basic HTTP log facility. Adjusted from here: http://stackoverflow.com/a/23660832/366313
	/// </summary>
	public class LogRequestAndResponseHandler : DelegatingHandler
	{
		// We name the logger generically here.
		private static readonly ILog Log = LogManager.GetLogger("Arsenal.Service");

		protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
			CancellationToken cancellationToken)
		{
			// Logging request body
			var requestBody = await request.Content.ReadAsStringAsync();
			Log.Info(requestBody);

			// Let other handlers process the request
			return await base.SendAsync(request, cancellationToken).ContinueWith(task =>
			{
				// Once response is ready, log it
			    if (task.Result.Content != null)
			    {
			        var responseBody = task.Result.Content.ReadAsStringAsync().Result;
                    Trace.WriteLine(responseBody);
                }

				return task.Result;
			}, cancellationToken);
		}
	}
}