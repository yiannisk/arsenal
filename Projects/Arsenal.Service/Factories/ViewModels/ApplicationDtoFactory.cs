﻿using System.Collections.Generic;
using System.Linq;
using Arsenal.DataAccess.Models;
using Arsenal.DataAccess.Repositories.Interfaces;
using Arsenal.Service.Factories.ViewModels.Interfaces;
using Arsenal.Service.Models;

namespace Arsenal.Service.Factories.ViewModels
{
	public class ApplicationDtoFactory : IApplicationDtoFactory
	{
		public ApplicationDtoFactory(IApplicationDependenciesRepository applicationDependenciesRepository,
			IApplicationDependencyDtoFactory applicationDependencyDtoFactory)
		{
			ApplicationDependenciesRepository = applicationDependenciesRepository;
			ApplicationDependencyDtoFactory = applicationDependencyDtoFactory;
		}

		private IApplicationDependenciesRepository ApplicationDependenciesRepository { get; set; }
		private IApplicationDependencyDtoFactory ApplicationDependencyDtoFactory { get; set; }

		public ApplicationDto Create(Application application)
		{
			var dependencies = ApplicationDependenciesRepository.GetByApplicationId(application.Id);

			return new ApplicationDto
			{
				Name = application.Name,
				Dependencies = ApplicationDependencyDtoFactory.CreateMany(dependencies)
			};
		}

		public IList<ApplicationDto> CreateMany(IList<Application> applications)
		{
			return applications.Select(Create).ToList();
		}
	}
}