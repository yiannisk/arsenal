﻿using System.Collections.Generic;
using System.Linq;
using Arsenal.DataAccess.Models;
using Arsenal.DataAccess.Repositories.Interfaces;
using Arsenal.Service.Factories.ViewModels.Interfaces;
using Arsenal.Service.Models;

namespace Arsenal.Service.Factories.ViewModels
{
	public class ApplicationDependencyDtoFactory : IApplicationDependencyDtoFactory
	{
		public ApplicationDependencyDtoFactory(IDependenciesRepository dependenciesRepository,
			IDependencyVersionsRepository dependencyVersionsRepository)
		{
			DependenciesRepository = dependenciesRepository;
			DependencyVersionsRepository = dependencyVersionsRepository;
		}

		private IDependenciesRepository DependenciesRepository { get; set; }
		private IDependencyVersionsRepository DependencyVersionsRepository { get; set; }

		public ApplicationDependencyDto Create(ApplicationDependency applicationDependency)
		{
			var dependencyVersion =
				DependencyVersionsRepository.GetByPrimaryKey(applicationDependency.DependencyVersionId);

			var latestVersion =
				DependencyVersionsRepository.GetByPrimaryKey(applicationDependency.LatestVersionId);

			var desiredVersion =
				DependencyVersionsRepository.GetByPrimaryKey(applicationDependency.DesiredVersionId);

			var dependency = DependenciesRepository.GetByPrimaryKey(dependencyVersion.DependencyId);

			return new ApplicationDependencyDto
			{
				Name = dependency.Name,
				CurrentVersion = dependencyVersion.VersionString,
				DesiredVersion = desiredVersion != null ? desiredVersion.VersionString : null,
				LatestVersion = latestVersion != null ? latestVersion.VersionString : null,
				CanBeUpdated = applicationDependency.CanBeUpdated,
				Reason = applicationDependency.Reason,
				Source = applicationDependency.Source,
				UpdatedAt = applicationDependency.UpdatedAt
			};
		}

		public IList<ApplicationDependencyDto> CreateMany(IList<ApplicationDependency> applicationDependencies)
		{
			return applicationDependencies.Select(Create).ToList();
		}
	}
}