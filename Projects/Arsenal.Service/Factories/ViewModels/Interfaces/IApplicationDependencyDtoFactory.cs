﻿using System.Collections.Generic;
using Arsenal.DataAccess.Models;
using Arsenal.Service.Models;

namespace Arsenal.Service.Factories.ViewModels.Interfaces
{
	public interface IApplicationDependencyDtoFactory
	{
		ApplicationDependencyDto Create(ApplicationDependency applicationDependency);
		IList<ApplicationDependencyDto> CreateMany(IList<ApplicationDependency> applicationDependencies);
	}
}