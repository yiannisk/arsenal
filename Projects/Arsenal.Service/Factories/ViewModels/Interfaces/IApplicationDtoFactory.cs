﻿using System.Collections.Generic;
using Arsenal.DataAccess.Models;
using Arsenal.Service.Models;

namespace Arsenal.Service.Factories.ViewModels.Interfaces
{
	public interface IApplicationDtoFactory
	{
		ApplicationDto Create(Application application);
		IList<ApplicationDto> CreateMany(IList<Application> applications);
	}
}