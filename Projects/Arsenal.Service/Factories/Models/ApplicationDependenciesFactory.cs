﻿using System;
using System.Collections.Generic;
using System.Linq;
using Arsenal.DataAccess.Models;
using Arsenal.DataAccess.Repositories.Interfaces;
using Arsenal.Service.Extensions;
using Arsenal.Service.Factories.Models.Interfaces;
using Arsenal.Service.Models;

namespace Arsenal.Service.Factories.Models
{
	public class ApplicationDependenciesFactory : IApplicationDependenciesFactory
	{
		public IDependencyVersionsRepository DependencyVersionsRepository { get; set; }
		public IDependenciesRepository DependenciesRepository { get; set; }
		public IApplicationDependenciesRepository ApplicationDependenciesRepository { get; set; }

		public ApplicationDependenciesFactory(IDependencyVersionsRepository dependencyVersionsRepository,
			IDependenciesRepository dependenciesRepository,
			IApplicationDependenciesRepository applicationDependenciesRepository)
		{
			DependencyVersionsRepository = dependencyVersionsRepository;
			DependenciesRepository = dependenciesRepository;
			ApplicationDependenciesRepository = applicationDependenciesRepository;
		}

		public IList<ApplicationDependency> CreateMany(long applicationId, IList<ApplicationDependencyDto> dependencies)
		{
			return dependencies == null 
				? new List<ApplicationDependency>() 
				: dependencies.Select(x => Create(applicationId, x)).ToList();
		}

		public ApplicationDependency Create(long applicationId, ApplicationDependencyDto applicationDependencyDto)
		{
			var dependency = DependenciesRepository.GetByName(applicationDependencyDto.Name) 
						  ?? DependenciesRepository.Insert(new Dependency
						  {
							  Name = applicationDependencyDto.Name,
							  CreatedAt = DateTime.Now,
							  UpdatedAt = DateTime.Now
						  });

			var applicationDependency = ApplicationDependenciesRepository.Insert(new ApplicationDependency
			{
				ApplicationId = applicationId,
				DependencyVersionId = GetOrCreateVersionId(dependency.Id, applicationDependencyDto.CurrentVersion).GetValueOrDefault(0),
				LatestVersionId = GetOrCreateVersionId(dependency.Id, applicationDependencyDto.LatestVersion),
				DesiredVersionId = GetOrCreateVersionId(dependency.Id, applicationDependencyDto.DesiredVersion),
				CanBeUpdated = applicationDependencyDto.CanBeUpdated,
				Source = applicationDependencyDto.Source,
				CreatedAt = DateTime.Now,
				UpdatedAt = DateTime.Now
			});

			return applicationDependency;
		}

		private long? GetOrCreateVersionId(long dependencyId, string versionString)
		{
			if (string.IsNullOrWhiteSpace(versionString)) return null;

			var version = versionString.ToVersion();

			return (DependencyVersionsRepository.GetByIdAndVersion(dependencyId, versionString)
			       ?? DependencyVersionsRepository.Insert(new DependencyVersion
			       {
				       CreatedAt = DateTime.Now,
				       UpdatedAt = DateTime.Now,
				       DependencyId = dependencyId,
					   VersionString = versionString,
				       MajorVersion = version.MajorVersion,
					   MinorVersion = version.MinorVersion,
					   MajorRevision = version.MajorRevision,
					   MinorRevision = version.MinorRevision
			       })).Id;
		}
	}
}