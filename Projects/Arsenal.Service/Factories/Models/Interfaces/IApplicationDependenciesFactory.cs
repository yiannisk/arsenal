﻿using System.Collections.Generic;
using Arsenal.DataAccess.Models;
using Arsenal.Service.Models;

namespace Arsenal.Service.Factories.Models.Interfaces
{
	public interface IApplicationDependenciesFactory
	{
		IList<ApplicationDependency> CreateMany(long applicationId, IList<ApplicationDependencyDto> dependencies);
		ApplicationDependency Create(long applicationId, ApplicationDependencyDto applicationDependencyDto);
	}
}