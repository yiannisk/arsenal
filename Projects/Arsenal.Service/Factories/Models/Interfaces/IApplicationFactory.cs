﻿using Arsenal.DataAccess.Models;
using Arsenal.Service.Models;

namespace Arsenal.Service.Factories.Models.Interfaces
{
	public interface IApplicationFactory
	{
		Application Create(ApplicationDto applicationDto);
	}
}