﻿using System;
using Arsenal.DataAccess.Models;
using Arsenal.DataAccess.Repositories.Interfaces;
using Arsenal.Service.Factories.Models.Interfaces;
using Arsenal.Service.Models;

namespace Arsenal.Service.Factories.Models
{
	public class ApplicationFactory : IApplicationFactory
	{
		public IApplicationsRepository ApplicationsRepository { get; set; }
		public IApplicationDependenciesRepository ApplicationDependenciesRepository { get; set; }
		public IApplicationDependenciesFactory ApplicationDependenciesFactory { get; set; }

		public ApplicationFactory(IApplicationsRepository applicationsRepository,
			IApplicationDependenciesFactory applicationDependenciesFactory,
			IApplicationDependenciesRepository applicationDependenciesRepository)
		{
			ApplicationsRepository = applicationsRepository;
			ApplicationDependenciesFactory = applicationDependenciesFactory;
			ApplicationDependenciesRepository = applicationDependenciesRepository;
		}

		public Application Create(ApplicationDto applicationDto)
		{
			if (applicationDto == null) return null;

			var application = ApplicationsRepository.GetByName(applicationDto.Name) ?? new Application
			{
				CreatedAt =	DateTime.Now,
				Name = applicationDto.Name,
				UpdatedAt = DateTime.Now,
			};

			if (application.Id > 0)
			{
				ApplicationsRepository.Update(application);
			}
			else
			{
				application = ApplicationsRepository.Insert(application);
			}

			ApplicationDependenciesRepository.DeleteForApplicationId(application.Id);
			ApplicationDependenciesFactory.CreateMany(application.Id, applicationDto.Dependencies);

			return application;
		}
	}
}