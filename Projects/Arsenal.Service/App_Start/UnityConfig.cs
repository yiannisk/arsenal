using System.Web.Http;
using Arsenal.DataAccess.Helpers;
using Arsenal.DataAccess.Repositories;
using Arsenal.DataAccess.Repositories.Interfaces;
using Arsenal.Service.Factories.Models;
using Arsenal.Service.Factories.Models.Interfaces;
using Arsenal.Service.Factories.ViewModels;
using Arsenal.Service.Factories.ViewModels.Interfaces;
using Microsoft.Practices.Unity;
using Unity.WebApi;

namespace Arsenal.Service
{
	public static class UnityConfig
	{
		public static void RegisterComponents()
		{
			var container = new UnityContainer();

			// Repositories
			container.RegisterType<ISystemsRepository, SystemsRepository>(new HierarchicalLifetimeManager());
			container.RegisterType<IApplicationsRepository, ApplicationsRepository>(new HierarchicalLifetimeManager());
			container.RegisterType<IApplicationDependenciesRepository, ApplicationDependenciesRepository>(
				new HierarchicalLifetimeManager());
			container.RegisterType<IDependenciesRepository, DependenciesRepository>(new HierarchicalLifetimeManager());
			container.RegisterType<IDependencyVersionsRepository, DependencyVersionsRepository>(new HierarchicalLifetimeManager());

			// Builders
			container.RegisterType<IApplicationDtoFactory, ApplicationDtoFactory>(new HierarchicalLifetimeManager());
			container.RegisterType<IApplicationDependencyDtoFactory, ApplicationDependencyDtoFactory>(
				new HierarchicalLifetimeManager());
			container.RegisterType<IApplicationFactory, ApplicationFactory>(new HierarchicalLifetimeManager());
			container.RegisterType<IApplicationDependenciesFactory, ApplicationDependenciesFactory>(
				new HierarchicalLifetimeManager());

		    container.RegisterType<IDatabaseHelpers, DatabaseHelpers>(new HierarchicalLifetimeManager());

			GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
		}
	}
}