﻿using System.Net.Http.Headers;
using System.Web.Http;
using Arsenal.Service.Handlers;

namespace Arsenal.Service
{
	public static class WebApiConfig
	{
		public static void Register(HttpConfiguration config)
		{
			ConfigureHandlers(config);
			ConfigureRouting(config);
			ConfigureMiscellaneous(config);
		}

		private static void ConfigureMiscellaneous(HttpConfiguration config)
		{
			// Ensure we get JSON when requesting text/html.
			config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
		}

		private static void ConfigureHandlers(HttpConfiguration config)
		{
			config.MessageHandlers.Add(new LogRequestAndResponseHandler());
		}

		private static void ConfigureRouting(HttpConfiguration config)
		{
			config.MapHttpAttributeRoutes();

			config.Routes.MapHttpRoute(
				name: "DefaultApi",
				routeTemplate: "api/{controller}/{id}",
				defaults: new { id = RouteParameter.Optional });
		}
	}
}