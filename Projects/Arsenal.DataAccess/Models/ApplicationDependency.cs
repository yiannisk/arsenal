﻿using System;
using Dapper;

namespace Arsenal.DataAccess.Models
{
	[Table("ApplicationDependencies")]
	public class ApplicationDependency
	{
		public long Id { get; set; }
		public long ApplicationId { get; set; }
		public long DependencyVersionId { get; set; }
		public long? LatestVersionId { get; set; }
		public long? DesiredVersionId { get; set; }
		public bool? CanBeUpdated { get; set; }
		public string Reason { get; set; }
		public string Source { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime UpdatedAt { get; set; }
	}
}