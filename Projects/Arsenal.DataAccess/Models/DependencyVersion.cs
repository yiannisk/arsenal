﻿using System;
using Dapper;

namespace Arsenal.DataAccess.Models
{
	[Table("DependencyVersions")]
	public class DependencyVersion
	{
		public long Id { get; set; }
		public long DependencyId { get; set; }
		public string VersionString { get; set; }
		public int MajorVersion { get; set; }
		public int? MinorVersion { get; set; }
		public int? MajorRevision { get; set; }
		public int? MinorRevision { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime UpdatedAt { get; set; }
	}
}