﻿using System;
using Dapper;

namespace Arsenal.DataAccess.Models
{
	[Table("Dependencies")]
	public class Dependency
	{
		public long Id { get; set; }
		public string Name { get; set; }
		public string ReleaseNotes { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime UpdatedAt { get; set; }
	}
}