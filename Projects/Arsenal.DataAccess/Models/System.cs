﻿using System;
using Dapper;

namespace Arsenal.DataAccess.Models
{
	[Table("Systems")]
	public class System
	{
		public long Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public long TeamId { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime UpdatedAt { get; set; }
	}
}