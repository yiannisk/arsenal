﻿using System;
using Dapper;

namespace Arsenal.DataAccess.Models
{
	[Table("Applications")]
	public class Application
	{
		public long Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string Repository { get; set; }
		public string RepositoryType { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime UpdatedAt { get; set; }
	}
}