﻿using System;
using Dapper;

namespace Arsenal.DataAccess.Models
{
	[Table("Teams")]
	public class Team
	{
		public long Id { get; set; }
		public string Name { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime UpdatedAt { get; set; }
	}
}