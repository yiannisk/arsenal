﻿using Arsenal.DataAccess.Properties;
using Arsenal.DataAccess.Repositories.Interfaces;

namespace Arsenal.DataAccess.Repositories
{
	public class SystemsRepository : DapperTableRepository<Models.System>, ISystemsRepository
	{
		public SystemsRepository() : base(Settings.Default.Database) {}
	}
}