﻿using System.Collections.Generic;
using System.Linq;
using Arsenal.DataAccess.Models;
using Arsenal.DataAccess.Properties;
using Arsenal.DataAccess.Repositories.Interfaces;
using Dapper;

namespace Arsenal.DataAccess.Repositories
{
	public class ApplicationDependenciesRepository : DapperTableRepository<ApplicationDependency>,
		IApplicationDependenciesRepository
	{
		public ApplicationDependenciesRepository() : base(Settings.Default.Database) {}

		public IList<ApplicationDependency> GetByApplicationId(long applicationId)
		{
			using (var connection = OpenConnection())
				return connection
					.Query<ApplicationDependency>("Select * From ApplicationDependencies Where ApplicationId = @ApplicationId",
						new {ApplicationId = applicationId})
					.ToList();
		}

		public void DeleteForApplicationId(long id)
		{
			using(var connection = OpenConnection())
				GetByApplicationId(id).ToList().ForEach(x => connection.Delete(x));
		}
	}
}