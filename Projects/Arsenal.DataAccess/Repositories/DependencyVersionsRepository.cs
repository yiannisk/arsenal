﻿using System.Linq;
using Arsenal.DataAccess.Models;
using Arsenal.DataAccess.Properties;
using Arsenal.DataAccess.Repositories.Interfaces;
using Dapper;

namespace Arsenal.DataAccess.Repositories
{
	public class DependencyVersionsRepository : DapperTableRepository<DependencyVersion>, IDependencyVersionsRepository
	{
		public DependencyVersionsRepository() : base(Settings.Default.Database) {}
		public DependencyVersion GetByIdAndVersion(long id, string version)
		{
			using (var connection = OpenConnection())
				return connection
					.Query<DependencyVersion>("Select * from DependencyVersions Where DependencyId = @DependencyId And VersionString = @Version",
						new { DependencyId = id, Version = version })
					.FirstOrDefault();
		}
	}
}