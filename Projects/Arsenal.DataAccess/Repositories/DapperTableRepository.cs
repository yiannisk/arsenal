﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Arsenal.DataAccess.Repositories.Interfaces;
using Dapper;

namespace Arsenal.DataAccess.Repositories
{
	public class DapperTableRepository<T> : IDapperTableRepository<T> where T : class
	{
		private readonly string _connectionString;

		public DapperTableRepository(string connectionString)
		{
			_connectionString = connectionString;
		}

		/// <summary>
		///     Returns a list of all items.
		/// </summary>
		/// <returns>a list of all items</returns>
		public IList<T> GetAll()
		{
			using (var connection = OpenConnection())
				return connection.GetList<T>().ToList();
		}

		/// <summary>
		///     Returns a single item by primary key.
		/// </summary>
		/// <param name="id"></param>
		/// <returns>One item with the appropriate id, or null if that is not found.</returns>
		public T GetByPrimaryKey(object id)
		{
			using (var connection = OpenConnection())
				return connection.Get<T>(id);
		}

		/// <summary>
		///     Inserts a given data object.
		/// </summary>
		/// <param name="item"></param>
		/// <returns>The id of the row just inserted.</returns>
		public T Insert(T item)
		{
			using (var connection = OpenConnection())
			{
				var id = connection.Insert(item);
				return GetByPrimaryKey(id);
			}
		}

		/// <summary>
		///     Updates a given data object.
		/// </summary>
		/// <param name="item"></param>
		/// <returns>Number of rows affected.</returns>
		public int Update(T item)
		{
			using (var connection = OpenConnection())
				return connection.Update(item);
		}

		/// <summary>
		///     Determines whether a particular entity exists for a given id.
		/// </summary>
		/// <param name="id"></param>
		/// <returns>True or False, indicating if entity with given id exists.</returns>
		public bool Exists(object id)
		{
			return GetByPrimaryKey(id) != null;
		}

		/// <summary>
		///     Deletes a given entity from the database.
		/// </summary>
		/// <param name="item"></param>
		/// <returns>Number of rows affected.</returns>
		public int Delete(T item)
		{
			using (var connection = OpenConnection())
				return connection.Delete(item);
		}

		protected virtual IDbConnection OpenConnection()
		{
			var connection = new SqlConnection(_connectionString);
			connection.Open();
			return connection;
		}
	}
}