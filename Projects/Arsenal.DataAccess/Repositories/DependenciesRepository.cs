﻿using System.Linq;
using Arsenal.DataAccess.Models;
using Arsenal.DataAccess.Properties;
using Arsenal.DataAccess.Repositories.Interfaces;
using Dapper;

namespace Arsenal.DataAccess.Repositories
{
	public class DependenciesRepository : DapperTableRepository<Dependency>, IDependenciesRepository
	{
		public DependenciesRepository() : base(Settings.Default.Database) {}
		public Dependency GetByName(string name)
		{
			using (var connection = OpenConnection())
				return connection
					.Query<Dependency>("Select * From Dependencies Where Name = @Name", new {Name = name})
					.FirstOrDefault();
		}
	}
}