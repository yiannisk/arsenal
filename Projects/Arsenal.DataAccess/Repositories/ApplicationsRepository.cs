﻿using System.Linq;
using Arsenal.DataAccess.Models;
using Arsenal.DataAccess.Properties;
using Arsenal.DataAccess.Repositories.Interfaces;
using Dapper;

namespace Arsenal.DataAccess.Repositories
{
	public class ApplicationsRepository : DapperTableRepository<Application>, IApplicationsRepository
	{
		public ApplicationsRepository() : base(Settings.Default.Database) {}
		public Application GetByName(string name)
		{
			using (var connection = OpenConnection())
				return connection
					.Query<Application>("Select * From Applications Where Name = @Name", new { Name = name })
					.FirstOrDefault();
		}
	}
}