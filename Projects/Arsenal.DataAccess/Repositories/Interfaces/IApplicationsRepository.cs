﻿using Arsenal.DataAccess.Models;

namespace Arsenal.DataAccess.Repositories.Interfaces
{
	public interface IApplicationsRepository : IDapperTableRepository<Application> {
		Application GetByName(string name);
	}
}