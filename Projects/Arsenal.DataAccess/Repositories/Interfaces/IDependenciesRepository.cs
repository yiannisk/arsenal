﻿using Arsenal.DataAccess.Models;

namespace Arsenal.DataAccess.Repositories.Interfaces
{
	public interface IDependenciesRepository : IDapperTableRepository<Dependency> {
		Dependency GetByName(string name);
	}
}