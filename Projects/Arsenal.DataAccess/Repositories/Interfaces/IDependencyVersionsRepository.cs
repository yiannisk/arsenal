﻿using Arsenal.DataAccess.Models;

namespace Arsenal.DataAccess.Repositories.Interfaces
{
	public interface IDependencyVersionsRepository : IDapperTableRepository<DependencyVersion> 
	{
		DependencyVersion GetByIdAndVersion(long id, string version);
	}
}