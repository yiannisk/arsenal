using System.Collections.Generic;

namespace Arsenal.DataAccess.Repositories.Interfaces
{
	public interface IDapperTableRepository<T> where T : class
	{
		/// <summary>
		///     Returns a list of all items.
		/// </summary>
		/// <returns>An <see cref="IList{T}
		///     <typeparam name="T"></typeparam>
		///     "/> with all items in the specified repo.
		/// </returns>
		IList<T> GetAll();

		/// <summary>
		///     Returns a single item by primary key.
		/// </summary>
		/// <param name="id"></param>
		/// <returns>One item with the appropriate id, or null if that is not found.</returns>
		T GetByPrimaryKey(object id);

		/// <summary>
		///     Inserts a given data object.
		/// </summary>
		/// <param name="item"></param>
		/// <returns>The id of the row just inserted.</returns>
		T Insert(T item);

		/// <summary>
		///     Updates a given data object.
		/// </summary>
		/// <param name="item"></param>
		/// <returns>Number of rows affected.</returns>
		int Update(T item);

		/// <summary>
		///     Determines whether a particular entity exists for a given id.
		/// </summary>
		/// <param name="id"></param>
		/// <returns>True or False, indicating if entity with given id exists.</returns>
		bool Exists(object id);

		/// <summary>
		///     Deletes a given entity from the database.
		/// </summary>
		/// <param name="item"></param>
		/// <returns>Number of rows affected.</returns>
		int Delete(T item);
	}
}