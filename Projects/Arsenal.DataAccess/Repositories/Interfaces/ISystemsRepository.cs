﻿namespace Arsenal.DataAccess.Repositories.Interfaces
{
	public interface ISystemsRepository : IDapperTableRepository<Models.System> {}
}