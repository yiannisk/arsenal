﻿using System.Collections.Generic;
using Arsenal.DataAccess.Models;

namespace Arsenal.DataAccess.Repositories.Interfaces
{
	public interface IApplicationDependenciesRepository : IDapperTableRepository<ApplicationDependency>
	{
		IList<ApplicationDependency> GetByApplicationId(long applicationId);
		void DeleteForApplicationId(long id);
	}
}