﻿using System;
using System.Data.SqlClient;
using Arsenal.DataAccess.Properties;

namespace Arsenal.DataAccess.Helpers
{
    public class DatabaseHelpers : IDatabaseHelpers
    {
        public bool CanConnect()
        {
            try
            {
                using (var connection = new SqlConnection(Settings.Default.Database))
                {
                    connection.Open();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
