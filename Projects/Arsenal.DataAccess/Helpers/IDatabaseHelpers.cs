﻿namespace Arsenal.DataAccess.Helpers
{
    public interface IDatabaseHelpers
    {
        bool CanConnect();
    }
}