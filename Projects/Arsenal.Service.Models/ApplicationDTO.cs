﻿using System.Collections.Generic;

namespace Arsenal.Service.Models
{
	public class ApplicationDto
	{
		public string Name { get; set; }
		public IList<ApplicationDependencyDto> Dependencies { get; set; }
	}
}