﻿using System;

namespace Arsenal.Service.Models
{
	public class ApplicationDependencyDto
	{
		public string Name { get; set; }
		public string CurrentVersion { get; set; }
		public string LatestVersion { get; set; }
		public string DesiredVersion { get; set; }
		public bool? CanBeUpdated { get; set; }
		public string Reason { get; set; }
		public string Source { get; set; }
		public DateTime UpdatedAt { get; set; }
	}
}